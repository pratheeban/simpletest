## WebRTC test 
### 1. Create a peer to peer application that allows several peers to send message to each other.
 - Designed a ```singlePage.html``` to  see the Callee and Caller peer in one page.
 - There are four minor changes in the existing minimalist code to achieve this single page chat.
      *``` callee.js``` and ```caller.js``` is called in same page. So I changed the variable names to avoid the conflict between the sending and receiving the data. Also, I have changed the appropriate element id name in the ```singlePage.html```.
      *   Since ```caller.js``` and ```callee.js``` use the same varaibale name  to send message ```channel.send(message)```. This leads to a conflict in receiving the text chat. So in ```callee.js``` at line 31, change the method name ```window.channel = receiveChannel;``` to ```window.channel2 = receiveChannel;```.
      *   For sometime the port 8090 dint work for me, so changed the port to 8089.
### 2.1 What are the problems of this architecture and describe an architecture that could solve those problems.?
   * It is possible to create a simple single WebRTC application, without any server components for signaling. In practice such application does not make much of a sense because it can be used only on a single page, thus it shares data among the same peer.

 * In the simple peer to peer case, it's kind off easy to establish connection and send or receive data. But when we add more number peers, then we need to choose an architecture, where every peer connects to every other peer. By mesh architecture, we can connect each peer with every peer other peer, but it get complicated for video or audio, because every peer has to send and copy this data to every other peer, this requires large bandwidth cost. So the number of peers supported in the topology is restricted to the bandwidth availability. The most robust architecture may be custom made MCU (Multipoint Control Unit), which centralise the data ( It records the information from all the connected peers and it pass to every other peers) and the advantage of this architecture is even one peer drops out, it wont affect the rest of the connections. 

I am not sure whether I answered your question properly. If not, Please correct me.
   
    

